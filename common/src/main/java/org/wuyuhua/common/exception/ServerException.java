package org.wuyuhua.common.exception;

/**
 * @author 伍玉华
 */
public class ServerException extends RuntimeException{

    /**
     * Constructs a {@code ServerException} with no detail message.
     */
    public ServerException() {
        super();
    }

    /**
     * Constructs a {@code ServerException} with the specified
     * detail message.
     *
     * @param   s   the detail message.
     */
    public ServerException(String s) {
        super(s);
    }

    /**
     * Constructs a new runtime exception with the specified detail message and
     * cause.  <p>Note that the detail message associated with
     * {@code cause} is <i>not</i> automatically incorporated in
     * this runtime exception's detail message.
     *
     * @param  message the detail message (which is saved for later retrieval
     *         by the {@link #getMessage()} method).
     * @param  cause the cause (which is saved for later retrieval by the
     *         {@link #getCause()} method).  (A <tt>null</tt> value is
     *         permitted, and indicates that the cause is nonexistent or
     *         unknown.)
     * @since  1.4
     */
    public ServerException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServerException(Throwable cause) {
        super(cause);
    }
}
