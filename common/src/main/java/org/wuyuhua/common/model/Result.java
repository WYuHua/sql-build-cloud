package org.wuyuhua.common.model;

import java.io.Serializable;

public class Result<T> implements Serializable {
    private static final long serialVersionUID = -4547662680378925782L;
    private T data;
    private Integer resp_code;
    private String resp_msg;

    public static <T> Result<T> succeed(String msg) {
        return of(null, CodeEnum.SUCCESS.getCode(), msg);
    }

    public static <T> Result<T> succeed(T model, String msg) {
        return of(model, CodeEnum.SUCCESS.getCode(), msg);
    }

    public static <T> Result<T> succeed(T model) {
        return of(model, CodeEnum.SUCCESS.getCode(), "");
    }

    public static <T> Result<T> of(T data, Integer code, String msg) {
        return new Result<>(data, code, msg);
    }

    public static <T> Result<T> failed(String msg) {
        return of(null, CodeEnum.ERROR.getCode(), msg);
    }

    public static <T> Result<T> failed(T model, String msg) {
        return of(model, CodeEnum.ERROR.getCode(), msg);
    }

    public static <T> Result<T> badRequest(String msg) {
        return of(null, CodeEnum.BADREQUEST.getCode(), msg);
    }

    public static <T> Result<T> badRequest(T model, String msg) {
        return of(model, CodeEnum.BADREQUEST.getCode(), msg);
    }

    public static <T> Result<T> unauthorized(String msg) {
        return of(null, CodeEnum.UNAUTHORIZED.getCode(), msg);
    }

    public static <T> Result<T> forbidden(T model, String msg) {
        return of(model, CodeEnum.UNAUTHORIZED.getCode(), msg);
    }

    public static <T> Result<T> forbidden(String msg) {
        return of(null, CodeEnum.FORBIDDEN.getCode(), msg);
    }

    public static <T> Result<T> unauthorized(T model, String msg) {
        return of(model, CodeEnum.FORBIDDEN.getCode(), msg);
    }

    public static <T> Result<T> notFound(String msg) {
        return of(null, CodeEnum.NOTFOUND.getCode(), msg);
    }

    public static <T> Result<T> notFound(T model, String msg) {
        return of(model, CodeEnum.NOTFOUND.getCode(), msg);
    }

    public Result(T data, Integer resp_code, String resp_msg) {
        this.data = data;
        this.resp_code = resp_code;
        this.resp_msg = resp_msg;
    }

    @Override
    public String toString() {
        return "R(code=" + this.getResp_code() + ", msg=" + this.getResp_msg() + ", data=" + this.getData() + ")";
    }

    public T getData() {
        return this.data;
    }

    public Integer getResp_code() {
        return this.resp_code;
    }

    public String getResp_msg() {
        return this.resp_msg;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void setResp_code(Integer resp_code) {
        this.resp_code = resp_code;
    }

    public void setResp_msg(String resp_msg) {
        this.resp_msg = resp_msg;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof Result)) {
            return false;
        } else {
            Result<?> other = (Result<?>)o;
            if (!other.canEqual(this)) {
                return false;
            } else {
                label47: {
                    Object this$resp_code = this.getResp_code();
                    Object other$resp_code = other.getResp_code();
                    if (this$resp_code == null) {
                        if (other$resp_code == null) {
                            break label47;
                        }
                    } else if (this$resp_code.equals(other$resp_code)) {
                        break label47;
                    }

                    return false;
                }

                Object this$data = this.getData();
                Object other$data = other.getData();
                if (this$data == null) {
                    if (other$data != null) {
                        return false;
                    }
                } else if (!this$data.equals(other$data)) {
                    return false;
                }

                Object this$resp_msg = this.getResp_msg();
                Object other$resp_msg = other.getResp_msg();
                if (this$resp_msg == null) {
                    if (other$resp_msg != null) {
                        return false;
                    }
                } else if (!this$resp_msg.equals(other$resp_msg)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof Result;
    }

    public int hashCode() {
        int result = 1;
        Object $resp_code = this.getResp_code();
        result = result * 59 + ($resp_code == null ? 43 : $resp_code.hashCode());
        Object $data = this.getData();
        result = result * 59 + ($data == null ? 43 : $data.hashCode());
        Object $resp_msg = this.getResp_msg();
        result = result * 59 + ($resp_msg == null ? 43 : $resp_msg.hashCode());
        return result;
    }

    public Result() {
    }
}
