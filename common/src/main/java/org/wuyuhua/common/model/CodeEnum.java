package org.wuyuhua.common.model;

public enum CodeEnum {

    SUCCESS(200, "成功"),
    FORBIDDEN(403, "禁止"),
    UNAUTHORIZED(401, "未授权"),
    NOTFOUND(404, "未找到"),
    SERVERERROR(500, "请求异常"),
    BADREQUEST(400, "请求失败"),
    ERROR(1, "错误");

    private final Integer code;
    private final String msg;

    private CodeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return this.code;
    }

    public String getMsg() {
        return this.msg;
    }
}
