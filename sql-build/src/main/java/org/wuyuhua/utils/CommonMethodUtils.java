package org.wuyuhua.utils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.util.StringUtils;

public class CommonMethodUtils {
    public  static <T> void buildQuery(QueryWrapper<T> query, String name, String startTime, String endTime){
        if (StringUtils.hasText(name)){
            query.like("name",name);
        }
        if (StringUtils.hasText(startTime)){
            query.ge("create_time",startTime);
        }
        if (StringUtils.hasText(endTime)){
            query.le("create_time",endTime);
        }
    }
}
