package org.wuyuhua.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.wuyuhua.common.model.Result;
import org.wuyuhua.model.database.po.DataSourceModel;
import org.wuyuhua.model.sqlnuild.dto.DataBaseDTO;
import org.wuyuhua.model.sqlnuild.dto.SearchResultDTO;
import org.wuyuhua.model.sqlnuild.dto.SqlBuildDTO;
import org.wuyuhua.model.sqlnuild.dto.TableDTO;
import org.wuyuhua.server.database.DataSourceService;
import org.wuyuhua.server.sqlbuild.SearchService;
import org.wuyuhua.server.sqlbuild.SqlBuildService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author 伍玉华
 */
@Slf4j
@RestController
@RequestMapping("/api/sql-build/")
public class SqlBuildController {

    private final SqlBuildService sqlBuildService;
    private final SearchService searchService;
    private final DataSourceService dataSourceService;
    public SqlBuildController(SqlBuildService sqlBuildService,
                              SearchService searchService,
                              DataSourceService dataSourceService) {
        this.sqlBuildService = sqlBuildService;
        this.searchService = searchService;
        this.dataSourceService = dataSourceService;
    }

    @PostMapping("create")
    public Result<String> createSql(@RequestBody SqlBuildDTO sqlBuild){
        try {
            return Result.succeed(sqlBuildService.create(sqlBuild),"构建成功!!!");
        }catch (Exception e){
            log.error("{}",e.getMessage(),e);
            return Result.failed(e.getMessage());
        }
    }

    @PutMapping("database/switch/{name}")
    public Result<String> switchDatasource(@PathVariable(value = "name") String name,HttpServletRequest request){
        try {
            dataSourceService.setDatasource(name,request);
            return Result.succeed("data source switch successfully.");
        }catch (Exception e){
            return Result.failed(e.getMessage());
        }
    }


    @PostMapping("database/test")
    public Result<String> test(@RequestBody DataSourceModel param){
        try {
            dataSourceService.test(param.getDataSourceName());
            return Result.succeed(param.getDataSourceName() + " data source connection successfully!");
        } catch (Exception e){
            log.error("{}",e.getMessage(),e);
            return Result.failed(e.getMessage());
        }
    }

    @GetMapping("test")
    public Result<String> test(@RequestParam(required = false) String name){
        try {
            dataSourceService.test(name);
            return Result.succeed(name + " data source connection successfully!");
        } catch (Exception e){
            log.error("{}",e.getMessage(),e);
            return Result.failed(e.getMessage());
        }
    }

    @GetMapping("database/get")
    public Result<String> getDataSource(HttpServletRequest request){
        return Result.succeed(dataSourceService.getNowDataSource(request),"successfully");
    }

    @GetMapping("search/data")
    public Result<DataBaseDTO> getData(HttpServletRequest request){
        try {
            return Result.succeed(searchService.getData(request));
        }catch (Exception e){
            log.error("error: {}",e.getMessage(),e);
            return Result.failed(e.getMessage());
        }
    }

    @GetMapping("search/tables")
    public Result<List<TableDTO>> tables(HttpServletRequest request){
        try {
            return Result.succeed(searchService.getTables(request));
        }catch (Exception e){
            return Result.failed(e.getMessage());
        }
    }

    @GetMapping("search/fields")
    public Result<List<String>> fields(@RequestParam String tableName,HttpServletRequest request){
        try {
            return Result.succeed(searchService.getFields(tableName,request));
        }catch (Exception e){
            return Result.failed(e.getMessage());
        }
    }

    @PostMapping("search/getDataBySql")
    public Result<SearchResultDTO> getDataBySql(@RequestBody Map<String,String> param,HttpServletRequest request){
        try {
            return Result.succeed(searchService.getDataBySql(param,request));
        }catch (Exception e){
            return Result.failed(e.getMessage());
        }
    }

    @PostMapping("search/getCreateTableDdl")
    public Result<List<Object>> getCreateTableDdl(){
        try {
            return Result.succeed(searchService.getCreateTableDdl());
        } catch (Exception e){
            return Result.failed(e.getMessage());
        }
    }
}
