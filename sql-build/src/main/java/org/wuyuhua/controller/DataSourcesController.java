package org.wuyuhua.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.wuyuhua.common.model.Result;
import org.wuyuhua.model.database.po.DataSourceModel;
import org.wuyuhua.model.database.po.SqlModel;
import org.wuyuhua.server.database.DataSourceService;
import org.wuyuhua.server.database.SqlService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/api/data-base/manager/")
public class DataSourcesController {

    private final DataSourceService dataSourceService;
    private final SqlService sqlService;

    public DataSourcesController(DataSourceService dataSourceService, SqlService sqlService) {
        this.dataSourceService = dataSourceService;
        this.sqlService = sqlService;
    }

    /**
     * 查询全部数据源列表
     * @return Result
     */
    @GetMapping("findDataSourceAll")
    public Result<List<DataSourceModel>> findDataSourceAll(HttpServletRequest request){
        try {
            return Result.succeed(dataSourceService.findDataSourceAll(request));
        }catch (Exception e){
            log.error("{}",e.getMessage(),e);
            return Result.failed(e.getMessage());
        }
    }

    /**
     * 分页查询数据源
     * @param pageSize 页面大小
     * @param pageNum 页码
     * @param name 数据源名称
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return Result
     */
    @GetMapping("findDataSource")
    public Result<Page<DataSourceModel>> findDataSource(@RequestParam Integer pageSize,
                                                        @RequestParam Integer pageNum,
                                                        @RequestParam(required = false) String name,
                                                        @RequestParam(required = false) String startTime,
                                                        @RequestParam(required = false) String endTime){
        try {
            return Result.succeed(dataSourceService.findDataSource(pageSize,pageNum,name,startTime,endTime));
        }catch (Exception e){
            log.error("DataSourceController-findDataSource:{}",e.getMessage(),e);
            return  Result.failed(e.getMessage());
        }
    }

    /**
     * 保存数据源
     * @param model 数据源参数
     * @return Result
     */
    @PutMapping("saveDataSource")
    public Result<DataSourceModel> saveDataSource(@RequestBody DataSourceModel model,HttpServletRequest request){
        try {
            return Result.succeed(dataSourceService.saveDataSource(model,request));
        }catch (Exception e){
            log.error("DataSourceController-saveDataSource:{}",e.getMessage(),e);
            return Result.failed(e.getMessage());
        }
    }

    /**
     * 删除数据源
     * @param name 数据源名称
     * @return Result
     */
    @DeleteMapping("delDataSource/{name}")
    public Result<DataSourceModel> delDataSource(@PathVariable(value = "name") String name,HttpServletRequest request){
        try {
            DataSourceModel one = dataSourceService.delDataSource(name,request);
            if (one!=null){
                return Result.succeed(one,"数据源已删除");
            } else {
                return Result.succeed("未找到数据源信息");
            }
        }catch (Exception e){
            log.error("DataSourceController-delDataSource:{}",e.getMessage(),e);
            return  Result.failed(e.getMessage());
        }
    }

    @PutMapping("closeDataSource/{name}")
    public Result<String> close(@PathVariable(value = "name") String name,HttpServletRequest request){
        try {
            dataSourceService.delDataSource(name,request);
            return Result.succeed("数据源已关闭,如需使用请重新注册");
        }catch (Exception e){
            log.error("DataSourceController-close:{}",e.getMessage(),e);
            return Result.failed(e.getMessage());
        }
    }


    @GetMapping("findSql")
    public Result<Page<SqlModel>> findSql(@RequestParam Integer pageSize,
                                          @RequestParam Integer pageNum,
                                          @RequestParam(required = false) Long dataSourceId,
                                          @RequestParam(required = false) String name,
                                          @RequestParam(required = false) String startTime,
                                          @RequestParam(required = false) String endTime,
                                          @RequestParam(required = false) String dataSourceName){
        try {
            return Result.succeed(sqlService.findSql(pageSize,pageNum,dataSourceId,name,startTime,endTime,dataSourceName));
        }catch (Exception e){
            log.error("{}",e.getMessage(),e);
            return Result.failed(e.getMessage());
        }
    }

    @GetMapping("findSqlByIds")
    public Result<List<SqlModel>> findSqlByIds(@RequestParam List<Long> ids){
        try {
            return Result.succeed(sqlService.listByIds(ids));
        }catch (Exception e){
            return Result.failed(e.getMessage());
        }
    }

    @PutMapping("sql/save")
    public Result<SqlModel> save(@RequestBody SqlModel model){
        try {
            return Result.succeed(sqlService.saveSqlModel(model));
        }catch (Exception e){
            log.error("{}",e.getMessage(),e);
            return Result.failed(e.getMessage());
        }
    }

    @DeleteMapping("delSqlModel")
    public Result<List<SqlModel>> delSqlModel(@RequestParam List<Long> ids){
        try {
            List<SqlModel> sqlModels = sqlService.listByIds(ids);
            if (!sqlService.removeByIds(ids)) {
                return Result.failed("删除失败,请检查参数和运行日志");
            }
            return Result.succeed(sqlModels,"成功删除以下数据: " + sqlModels.stream().map(SqlModel::getName).collect(Collectors.joining(",")));
        }catch (Exception e){
            return Result.failed(e.getMessage());
        }
    }
}
