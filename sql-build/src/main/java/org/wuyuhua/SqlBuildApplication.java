package org.wuyuhua;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 伍玉华
 * @date 2024-03-31
 */
@SpringBootApplication
public class SqlBuildApplication
{
    public static void main( String[] args )
    {
        SpringApplication.run(SqlBuildApplication.class,args);
    }
}
