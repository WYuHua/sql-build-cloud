package org.wuyuhua.server.sqlbuild.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.wuyuhua.common.exception.ServerException;
import org.wuyuhua.mapper.datasource.SqlMapper;
import org.wuyuhua.model.database.po.SqlModel;
import org.wuyuhua.model.sqlnuild.dto.*;
import org.wuyuhua.server.sqlbuild.SqlBuildService;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SqlBuildServiceImpl implements SqlBuildService {


    private final SqlMapper sqlMapper;

    private static final String POINT = ".";
    private static final String COMMA = ",";
    private static final String BLANK = " ";
    private static final String EMPTY = "''";

    public SqlBuildServiceImpl(SqlMapper sqlMapper) {
        this.sqlMapper = sqlMapper;
    }

    @Override
    public String create(SqlBuildDTO sqlBuild) {

        if (CollectionUtils.isEmpty(sqlBuild.getTableSearch())){
            throw new ServerException("待查询的表为空!");
        }

        if (CollectionUtils.isEmpty(sqlBuild.getFieldSearch())){
            throw new ServerException("待查询的列为空!");
        }

        Map<String, TableSearchDTO> tableMap = buildTableMap(sqlBuild.getTableSearch(),
                sqlBuild.getFieldSearch(),
                sqlBuild.getFilterSearch(),
                sqlBuild.getOtherCondition());

        String select = buildSelect(sqlBuild.getFieldSearch(), tableMap);
        String from = buildFrom(sqlBuild.getTableSearch(), sqlBuild.getTableAssociation(),tableMap);

        String where = "";
        if (!CollectionUtils.isEmpty(sqlBuild.getFilterSearch())){
            where = buildWhere(sqlBuild.getFilterSearch(), sqlBuild.getFilterAssociation(), tableMap);
        }

        String other = "";
        if (!CollectionUtils.isEmpty(sqlBuild.getOtherCondition())) {
           other = buildOther(sqlBuild.getOtherCondition(),tableMap);
        }

        String sql = select + "\n" + from + "\n" + where + "\n" + other;

        // 替换子查询
        List<Long> sqlIds = new ArrayList<>();

        for (Map.Entry<String, TableSearchDTO> entry : tableMap.entrySet()) {
            TableSearchDTO table = entry.getValue();
            if (table.getSqlId() != -1){
                sqlIds.add(table.getSqlId().longValue());
            }
        }

        if (sqlIds.isEmpty()) {
            return sql;
        }

        try {
            List<SqlModel> data = sqlMapper.selectBatchIds(sqlIds);
            for (SqlModel sqlItem : data) {
                String content = "(" + sqlItem.getContent() + ")";
                String sqlName = "#{"+sqlItem.getName()+"}";
                sql = sql.replace(sqlName,content);
            }

        } catch (ServerException e){
            throw new ServerException(e.getMessage());
        } catch (Exception e){
            log.error("{}-{}",this.getClass().getName(),e.getMessage(),e);
            throw new ServerException("远程调用接口失败: "+e.getMessage());
        }

        return sql;
    }


    private Map<String,TableSearchDTO> buildTableMap(List<TableSearchDTO> tableSearch,
                                                     List<FieldSearchDTO> fieldSearch,
                                                     List<FilterSearchDTO> filterSearch,
                                                     List<OtherConditionDTO> otherCondition){
        Map<String,TableSearchDTO> map = new HashMap<>(tableSearch.size());
        for (TableSearchDTO item : tableSearch) {
            map.put(item.getName(),item);
        }

        if (!CollectionUtils.isEmpty(fieldSearch)) {
            for (FieldSearchDTO search : fieldSearch) {
                TableSearchDTO table = search.getTable();
                map.putIfAbsent(table.getName(), table);
            }
        }

        if (!CollectionUtils.isEmpty(filterSearch)) {
            for (FilterSearchDTO search : filterSearch) {
                if (CollectionUtils.isEmpty(search.getFilters())){
                    continue;
                }
                for (FilterItemDTO filter : search.getFilters()) {
                    TableSearchDTO sql = filter.getSql();
                    if (sql!=null){
                        map.putIfAbsent(sql.getName(), sql);
                    }
                }
            }
        }

        if (!CollectionUtils.isEmpty(otherCondition)) {
            for (OtherConditionDTO otherConditionDTO : otherCondition) {
                if (CollectionUtils.isEmpty(otherConditionDTO.getFields())) {
                    continue;
                }
                for (FieldSearchDTO field : otherConditionDTO.getFields()) {
                    map.putIfAbsent(field.getTable().getName(),field.getTable());
                }
            }
        }

        return map;
    }

    /**
     * 构建Select 语句
     * @param fieldSearch 需要查询的属性
     * @param tableMap 表格Map
     * @return SELECT ...
     */
    private String buildSelect(List<FieldSearchDTO> fieldSearch,Map<String, TableSearchDTO> tableMap){
        StringBuilder select = new StringBuilder("SELECT ");
        for (int i = 0; i < fieldSearch.size(); i++) {

            FieldSearchDTO field = fieldSearch.get(i);

            // 构建 表名.列名
            String fieldName = getFieldName(field,tableMap);

            // 替换成自定义
            if (StringUtils.hasText(field.getCustom())){
                fieldName = field.getCustom().replace("#",fieldName);
            }

            // 加上别名
            if (StringUtils.hasText(field.getNickname())){
                fieldName = fieldName + " as " + field.getNickname();
            }

            select.append(fieldName);

            if (i < fieldSearch.size() -1 ){
                select.append(COMMA);
            }
        }
        return select.toString();
    }


    /**
     * 构建From 语句
     * @param tableSearch 需要查询的表格列表
     * @param tableAssociation 表格的关系
     * @param tableMap 表格Map
     * @return FROM ...
     */
    private String buildFrom(List<TableSearchDTO> tableSearch,
                             List<TableAssociationDTO> tableAssociation,
                             Map<String, TableSearchDTO> tableMap){
        StringBuilder result = new StringBuilder("FROM ");

        if (CollectionUtils.isEmpty(tableAssociation)){
            for (int i = 0; i < tableSearch.size(); i++) {

                TableSearchDTO table = tableSearch.get(i);
                String tableName = table.getName();

                if (StringUtils.hasText(table.getNickname())){
                    tableName = tableName + BLANK + table.getNickname();
                }

                result.append(tableName);

                if (i<tableSearch.size() - 1){
                    result.append(COMMA);
                }
            }

            return result.toString();
        }

        for (int i = 0; i < tableAssociation.size(); i++) {
            TableAssociationDTO association = tableAssociation.get(i);

            if (i == 0){
                TableSearchDTO master = tableMap.get(association.getMaster().getName());
                result.append(getTableNameAndTableNickName(master));
            }

            result.append(BLANK).append(association.getConnType()).append(BLANK);

            TableSearchDTO slave = tableMap.get(association.getSlave().getName());
            result.append(getTableNameAndTableNickName(slave));

            result.append(" ON ");

            String left = getFieldName(association.getLeft(),tableMap);
            result.append(left);

            result.append(" = ");

            String right = getFieldName(association.getRight(),tableMap);
            result.append(right);
        }

        return result.toString();
    }


    /**
     * 构建Where 语句
     * @param filterSearch 筛选条件
     * @param filterAssociation 筛选条件关联
     * @param tableMap 表格Map
     * @return Where ...
     */
    private String buildWhere(List<FilterSearchDTO> filterSearch,
                              List<FilterAssociationDTO> filterAssociation,
                              Map<String, TableSearchDTO> tableMap){
        StringBuilder result = new StringBuilder("WHERE ");
        Map<String, String> filterMap = buildFilterMap(filterSearch, tableMap);

        if (CollectionUtils.isEmpty(filterAssociation)){
            for (int i = 0; i < filterSearch.size(); i++) {
                FilterSearchDTO filter = filterSearch.get(i);

                String str = filterMap.get(filter.getName());

                result.append(str);

                if (i < filterSearch.size() - 1){
                    result.append(" and ");
                }
            }
        }

        for (int i = 0; i < filterAssociation.size(); i++) {
            FilterAssociationDTO association = filterAssociation.get(i);

            StringBuilder item = new StringBuilder();
            item.append("(");
            setFilterAssociation(association, item, filterMap);
            item.append(")");


            if (i < filterAssociation.size()-1){
                if (association.getType() == 1){
                    item.append(" and ");
                }else{
                    item.append(" or ");
                }
            }

            result.append(item);
        }

        return result.toString();
    }


    private String buildOther(List<OtherConditionDTO> otherCondition,Map<String, TableSearchDTO> tableMap){

        StringBuilder result = new StringBuilder();

        // 处理分组
        for (int i = 0; i < otherCondition.size(); i++) {
            OtherConditionDTO otherConditionDTO = otherCondition.get(i);

            if (!"GROUP BY".equals(otherConditionDTO.getTypeName().trim().toUpperCase(Locale.ROOT))){
                continue;
            }

            setOtherCondition(otherCondition, tableMap, result, i, otherConditionDTO);
        }


        // 特殊处理排序
        List<OtherConditionDTO> order = otherCondition.stream().filter(k -> "ORDER BY".equals(k.getTypeName().trim().toUpperCase(Locale.ROOT))).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(order)){
            for (int i = 0; i < order.size(); i++) {
                OtherConditionDTO item = order.get(i);
                if (i == 0){
                    result.append(item.getTypeName()).append(BLANK);
                }
                applyOtherFields(tableMap, result, item);

                result.append(item.getContent());

                if (i < order.size() - 1){
                    result.append(COMMA);
                }
            }
            result.append("\n");
        }

        for (int i = 0; i < otherCondition.size(); i++) {
            OtherConditionDTO otherConditionDTO = otherCondition.get(i);

            // 跳过排序处理
            if ("ORDER BY".equals(otherConditionDTO.getTypeName().trim().toUpperCase(Locale.ROOT))){
                continue;
            }

            // 跳过分组处理
            if ("GROUP BY".equals(otherConditionDTO.getTypeName().trim().toUpperCase(Locale.ROOT))){
                continue;
            }

            setOtherCondition(otherCondition, tableMap, result, i, otherConditionDTO);
        }

        return result.toString();
    }

    private void setOtherCondition(List<OtherConditionDTO> otherCondition, Map<String, TableSearchDTO> tableMap, StringBuilder result, int i, OtherConditionDTO otherConditionDTO) {
        result.append(otherConditionDTO.getTypeName()).append(BLANK);

        applyOtherFields(tableMap, result, otherConditionDTO);

        if (StringUtils.hasText(otherConditionDTO.getContent())){
            result.append(otherConditionDTO.getContent());
        }

        if (i < otherCondition.size() - 1){
            result.append("\n");
        }
    }

    private void applyOtherFields(Map<String, TableSearchDTO> tableMap, StringBuilder result, OtherConditionDTO item) {
        if (!CollectionUtils.isEmpty(item.getFields())){
            for (int j = 0; j < item.getFields().size(); j++) {

                FieldSearchDTO field = item.getFields().get(j);
                result.append(getFieldName(field,tableMap));

                if (j < item.getFields().size() - 1){
                    result.append(COMMA);
                }
            }
            result.append(BLANK);
        }
    }


    private void setFilterAssociation(FilterAssociationDTO association,StringBuilder item,Map<String, String> filterMap){
        String filter = filterMap.get(association.getLabel());
        item.append(filter);
        if (!CollectionUtils.isEmpty(association.getChildren())){
            item.append(" and (");
            for (int i = 0; i < association.getChildren().size(); i++) {
                FilterAssociationDTO child = association.getChildren().get(i);
                boolean isNeedBracket = association.getChildren().size() > 1;

                if (isNeedBracket){
                    item.append("(");
                }

                setFilterAssociation(child,item,filterMap);

                if (isNeedBracket){
                    item.append(")");
                }

                if (i < association.getChildren().size() - 1){
                    if (child.getType() == 1){
                        item.append(" and ");
                    }else{
                        item.append(" or ");
                    }
                }
            }

            item.append(")");
        }

    }

    /**
     * 构建筛选条件Map
     * @param filterSearch 筛选条件列表
     * @return Map
     */
    private Map<String,String> buildFilterMap(List<FilterSearchDTO> filterSearch,
                                              Map<String, TableSearchDTO> tableMap){
        Map<String,String> map = new HashMap<>(filterSearch.size());

        for (FilterSearchDTO item : filterSearch) {
            List<FilterItemDTO> filters = item.getFilters();
            if (CollectionUtils.isEmpty(filters)){
                continue;
            }

            StringBuilder filterStr = new StringBuilder();
            for (int i = 0; i < filters.size(); i++) {
                FilterItemDTO filter = filters.get(i);

                try {
                    filter.setSymbol(filter.getSymbol().toUpperCase(Locale.ROOT));
                }catch (Exception e){
                    log.error("符号转换成大写失败: {}",filter.getSymbol());
                }

                boolean isBetween = "BETWEEN".equals(filter.getSymbol());

                if (isBetween){
                    filterStr.append("(");
                }

                if (filter.getLeft().getField()!=null){
                    filterStr.append(getFieldName(filter.getLeft(),tableMap));
                }

                filterStr.append(BLANK).append(filter.getSymbol()).append(BLANK);
                switch (filter.getSymbol()){
                    case "IN":
                        if (StringUtils.hasText(filter.getRightCustom())){
                            filterStr.append("(").append(filter.getRightCustom()).append(")");
                        } else {
                            filterStr.append(filter.getSql().getName());
                        }
                        break;
                    case "IS NULL":
                    case "IS NOT NULL":
                        break;
                    case "BETWEEN":

                        String[] split = StringUtils.hasText(filter.getRightCustom())?
                                filter.getRightCustom().split(COMMA):
                                new String[0];

                        switch (split.length){
                            case 0: filterStr.append(EMPTY).append(" AND ").append(EMPTY);
                                break;
                            case 1: filterStr.append(split[0]).append(" AND ").append(EMPTY);
                                break;
                            default: filterStr.append(split[0]).append(" AND ").append(split[1]);
                        }

                        break;

                    default:
                        String right = BLANK;
                        if (filter.getRight() != null) {
                            right = getFieldName(filter.getRight(),tableMap);
                        } else if (StringUtils.hasText(filter.getRightCustom())){
                            right = filter.getRightCustom();
                        } else if (filter.getSql() != null){
                            right = filter.getSql().getName() ;
                        }
                        filterStr.append(right);
                }

                if (isBetween){
                    filterStr.append(")");
                }

                if (i < filters.size() - 1){
                    filterStr.append(BLANK).append(filter.getConnector()).append(BLANK);
                }
            }
            map.put(item.getName(),filterStr.toString());
        }

        return map;
    }

    private String getFieldName(FieldSearchDTO field,Map<String, TableSearchDTO> tableMap){
        if (field.getTable() != null && field.getTable().getName()!=null){
            TableSearchDTO table = tableMap.get(field.getTable().getName());
            if (table == null){
                return "";
            }
            return StringUtils.hasText(table.getNickname())?
                    table.getNickname()+POINT+field.getField():
                    table.getName()+POINT+field.getField();
        }
        return "";
    }

    private String getTableNameAndTableNickName(TableSearchDTO table){
        return StringUtils.hasText(table.getNickname())?
                table.getName() + BLANK + table.getNickname():
                table.getName();
    }

    public static void main(String[] args) {

        SqlBuildDTO param = new SqlBuildDTO(
                new ArrayList<>(), new ArrayList<>(),new ArrayList<>(),
                new ArrayList<>(), new ArrayList<>(),new ArrayList<>());
        TableSearchDTO fileTable = new TableSearchDTO("查询全部文件","a",2,1);
        TableSearchDTO userTable = new TableSearchDTO("user","",1,-1);
        param.getTableSearch().add(fileTable);
        param.getTableSearch().add(userTable);

        FieldSearchDTO id = new FieldSearchDTO(fileTable,"id","","");
        FieldSearchDTO fileName = new FieldSearchDTO(fileTable,"name","fileName","");
        FieldSearchDTO path = new FieldSearchDTO(fileTable,"path","","sum(#)");
        FieldSearchDTO username = new FieldSearchDTO(userTable,"username","","");
        FieldSearchDTO upload = new FieldSearchDTO(fileTable,"upload","","");
        FieldSearchDTO userId = new FieldSearchDTO(userTable,"userId","","");
        FieldSearchDTO createTime = new FieldSearchDTO(fileTable,"createTime","","");
        param.getFieldSearch().add(id);
        param.getFieldSearch().add(fileName);
        param.getFieldSearch().add(path);
        param.getFieldSearch().add(username);
        param.getFieldSearch().add(upload);

        param.getTableAssociation().add(new TableAssociationDTO(fileTable,userTable,"LEFT JOIN",upload,userId));
        param.getTableAssociation().add(new TableAssociationDTO(fileTable,userTable,"LEFT JOIN",upload,userId));

        FilterItemDTO filter1 = new FilterItemDTO(upload,"=",userId,null,null,"and");
        FilterItemDTO filter2 = new FilterItemDTO(id,"in",null,"1,2,3,4",null,"and");
        FilterItemDTO filter3 = new FilterItemDTO(path,"like",null,"'%root%'",null,"and");
        FilterItemDTO filter4 = new FilterItemDTO(fileName,"is not null",null,null,null,"and");
        FilterItemDTO filter5 = new FilterItemDTO(createTime,"between",null,"'2023-09-01','2023-09-31'",null,"and");

        FilterSearchDTO filterSearch1 = new FilterSearchDTO("1",new ArrayList<>());
        filterSearch1.getFilters().add(filter1);
        filterSearch1.getFilters().add(filter2);

        FilterSearchDTO filterSearch2 = new FilterSearchDTO("2",new ArrayList<>());
        filterSearch2.getFilters().add(filter3);
        filterSearch2.getFilters().add(filter4);
        filterSearch2.getFilters().add(filter5);

        param.getFilterSearch().add(filterSearch1);
        param.getFilterSearch().add(filterSearch2);

        FilterAssociationDTO a = new FilterAssociationDTO("1","1",1,new ArrayList<>());
        FilterAssociationDTO b = new FilterAssociationDTO("2","2",1,new ArrayList<>());
        FilterAssociationDTO one = new FilterAssociationDTO("1","1",0,new ArrayList<>());
        FilterAssociationDTO two = new FilterAssociationDTO("2","2",0,new ArrayList<>());
        a.getChildren().add(one);
        a.getChildren().add(two);
        b.getChildren().add(two);
        b.getChildren().add(one);

        param.getFilterAssociation().add(a);
        param.getFilterAssociation().add(b);

        OtherConditionDTO condition1 = new OtherConditionDTO("GROUP BY", new ArrayList<>(),"");
        condition1.getFields().add(fileName);
        condition1.getFields().add(username);
        OtherConditionDTO condition2 = new OtherConditionDTO("LIMIT", new ArrayList<>(),"1,10");
        param.getOtherCondition().add(condition1);
        param.getOtherCondition().add(condition2);

//        System.out.println(impl.create(param));
    }
}
