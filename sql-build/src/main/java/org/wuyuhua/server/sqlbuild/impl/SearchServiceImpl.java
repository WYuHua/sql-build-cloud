package org.wuyuhua.server.sqlbuild.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.wuyuhua.common.exception.ServerException;
import org.wuyuhua.compent.DataBaseClient;
import org.wuyuhua.datasource.DataSourceBuild;
import org.wuyuhua.model.database.po.SqlModel;
import org.wuyuhua.model.sqlnuild.dto.*;
import org.wuyuhua.server.database.SqlService;
import org.wuyuhua.server.sqlbuild.SearchService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 伍玉华
 */
@Slf4j
@Service
public class SearchServiceImpl implements SearchService {

    private final DataBaseClient dataBaseClient;
    private final SqlService sqlService;

    public SearchServiceImpl(DataBaseClient dataBaseClient, SqlService sqlService) {
        this.dataBaseClient = dataBaseClient;
        this.sqlService = sqlService;
    }

    @NotNull
    public static String extractBetweenSelectAndFrom(String input) {
        // 使用正则表达式将 "select" 和 "from" 之间的内容提取出来
        String pattern = "(?i)select(.*?)from";
        Pattern r = Pattern.compile(pattern, Pattern.DOTALL);
        Matcher m = r.matcher(input);

        if (m.find()) {
            return m.group(1).trim();
        } else {
            return "";
        }
    }

    @Override
    public DataBaseDTO getData(HttpServletRequest request) {
        DataBaseDTO result = new DataBaseDTO();
        List<TableDTO> tables = getTables(request);


        List<FieldsDTO> fields = new ArrayList<>();
        TableDTO tableDTO = tables.get(0);
        if ("数据表".equals(tableDTO.getValue())) {
            List<ChildrenDTO> children = tableDTO.getChildren();
            if (!CollectionUtils.isEmpty(children)) {
                for (ChildrenDTO child : children) {
                    String tableName = child.getLabel();
                    FieldsDTO fieldsDTO = new FieldsDTO();
                    Object[] value = new Object[]{"数据表", child.getValue()};
                    fieldsDTO.setValue(value);
                    fieldsDTO.setLabel(tableName);

                    List<FieldValueDTO> childField = new ArrayList<>();

                    List<String> fieldValueList = getFields(tableName, request);

                    setFieldsChildren(fields, fieldsDTO, childField, fieldValueList);
                }
            }
        }
        if (tables.size() > 1) {
            TableDTO queryDTO = tables.get(1);
            if ("子查询".equals(queryDTO.getValue()) && !CollectionUtils.isEmpty(queryDTO.getChildren())) {
                List<ChildrenDTO> children = queryDTO.getChildren();
                for (ChildrenDTO child : children) {
                    String tableName = child.getLabel();
                    FieldsDTO fieldsDTO = new FieldsDTO();
                    Object[] value = new Object[]{"子查询", child.getValue()};
                    fieldsDTO.setValue(value);
                    fieldsDTO.setLabel(tableName);

                    List<FieldValueDTO> childField = new ArrayList<>();

                    List<String> fieldValueList = child.getFields();

                    setFieldsChildren(fields, fieldsDTO, childField, fieldValueList);
                }
            }
        }

        result.setTables(tables);
        result.setFields(fields);
        return result;
    }

    private void setFieldsChildren(List<FieldsDTO> fields, FieldsDTO fieldsDTO, List<FieldValueDTO> childField, List<String> fieldValueList) {
        if (CollectionUtils.isEmpty(fieldValueList)) {
            fieldsDTO.setChildren(childField);
            fields.add(fieldsDTO);
            return;
        }

        for (String field : fieldValueList) {
            FieldValueDTO dto = new FieldValueDTO();
            dto.setValue(field);
            dto.setLabel(field);
            childField.add(dto);
        }

        fieldsDTO.setChildren(childField);
        fields.add(fieldsDTO);
    }

    @Override
    public List<TableDTO> getTables(HttpServletRequest request) {
        List<TableDTO> result = new ArrayList<>();
        DataSourceBuild dataSourceBuild = dataBaseClient.getNowDataSourceBySession();
        List<String> tables = dataSourceBuild.tables();
        if (CollectionUtils.isEmpty(tables)) {
            return result;
        }
        TableDTO tableDTO = new TableDTO();
        tableDTO.setValue("数据表");
        tableDTO.setLabel("数据表");
        List<ChildrenDTO> children = new ArrayList<>();
        for (String table : tables) {
            ChildrenDTO dto = new ChildrenDTO();
            dto.setValue(new ValueDTO(-1, table));
            dto.setLabel(table);
            children.add(dto);
        }
        tableDTO.setChildren(children);
        result.add(tableDTO);

        try {
            Page<SqlModel> data = sqlService.findSql(-1, -1, null, null, null, null,dataSourceBuild.getDataBaseName());
            List<SqlModel> sqlList = data.getRecords();

            if (CollectionUtils.isEmpty(sqlList)) {
                return result;
            }

            TableDTO sqlDTO = new TableDTO();
            sqlDTO.setValue("子查询");
            sqlDTO.setLabel("子查询");
            sqlDTO.setChildren(new ArrayList<>());
            for (SqlModel sqlModel : sqlList) {
                ChildrenDTO dto = new ChildrenDTO();
                dto.setValue(new ValueDTO(sqlModel.getId().intValue(), sqlModel.getName()));
                dto.setLabel(sqlModel.getName());
                dto.setFields(getFieldBySql(sqlModel));
                sqlDTO.getChildren().add(dto);
            }
            result.add(sqlDTO);

        } catch (Exception e) {
            log.error("{}-{}", this.getClass().getName(), e.getMessage(), e);
            throw new ServerException("远程调用sql语句查询接口失败:" + e.getMessage());
        }

        return result;
    }

    @Override
    public List<String> getFields(String tableName, HttpServletRequest request) {
        DataSourceBuild dataSourceBuild = dataBaseClient.getNowDataSourceBySession();
        return dataSourceBuild.fields(tableName);
    }

    @Override
    public SearchResultDTO getDataBySql(Map<String, String> param, HttpServletRequest request) {
        String sql = param.get("sql");
        DataSourceBuild dataSourceBuild = dataBaseClient.getNowDataSourceBySession();
        if (!StringUtils.hasText(sql)) {
            throw new ServerException("sql语句为空");
        }
        try {
            return dataSourceBuild.getDataBySql(sql);
        } catch (DataAccessException e) {
            throw new ServerException(e);
        }
    }

    @Override
    public List<Object> getCreateTableDdl() {
        List<Object> lists = new ArrayList<>();
        DataSourceBuild dataSourceBuild = dataBaseClient.getNowDataSourceBySession();
        List<String> tables = dataSourceBuild.tables();
        for (String table : tables) {
            try {
                Object tableDdl = dataSourceBuild.getCreateTableDdl(table);
                lists.add(tableDdl);
            } catch (Exception e){
                log.error("getCreateTableDdl:{}",table,e);
            }

        }
        return lists;
    }

    /**
     * 根据sql语句获取sql的查询列表
     *
     * @param sqlModel sql对象
     * @return List
     */
    private List<String> getFieldBySql(SqlModel sqlModel) {
        List<String> result = new ArrayList<>();

        String content = sqlModel.getContent();

        if (!StringUtils.hasText(content)) {
            return result;
        }

        String[] s = extractBetweenSelectAndFrom(content).split(",");

        for (String field : s) {
            field = field.trim();
            int lastIndexOf = field.lastIndexOf(" ");
            if (lastIndexOf != -1) {
                result.add(field.substring(lastIndexOf + 1));
            } else {
                result.add(field);
            }
        }

        return result;
    }
}
