package org.wuyuhua.server.sqlbuild;


import org.wuyuhua.model.sqlnuild.dto.SqlBuildDTO;

public interface SqlBuildService {

    /**
     * 创建sql
     * @param sqlBuild sqlBuild
     * @return String
     */
    String create(SqlBuildDTO sqlBuild);
}
