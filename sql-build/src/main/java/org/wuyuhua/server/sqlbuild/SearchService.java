package org.wuyuhua.server.sqlbuild;


import org.wuyuhua.model.sqlnuild.dto.DataBaseDTO;
import org.wuyuhua.model.sqlnuild.dto.SearchResultDTO;
import org.wuyuhua.model.sqlnuild.dto.TableDTO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

public interface SearchService {

    /**
     * 获取数据
     * @param request 当前请求
     * @return DataBaseDTO
     */
    DataBaseDTO getData(HttpServletRequest request);

    /**
     * 获取表
     * @param request 当前请求
     * @return List<TableDTO>
     */
    List<TableDTO> getTables(HttpServletRequest request);

    /**
     * 获取属性
     * @param request 当前请求
     * @param tableName 表名
     * @return List<String>
     */
    List<String> getFields(String tableName,HttpServletRequest request);

    /**
     * 执行sql查询数据
     * @param request 当前请求
     * @param param 参数
     * @return SearchResultDTO
     */
    SearchResultDTO getDataBySql(Map<String,String> param,HttpServletRequest request);

    /**
     * 获取创建表的ddl语句
     * @return Object
     */
    List<Object> getCreateTableDdl();
}
