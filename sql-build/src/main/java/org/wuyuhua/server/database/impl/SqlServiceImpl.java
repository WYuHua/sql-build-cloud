package org.wuyuhua.server.database.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.wuyuhua.mapper.datasource.SqlMapper;
import org.wuyuhua.model.database.po.DataSourceModel;
import org.wuyuhua.model.database.po.SqlModel;
import org.wuyuhua.server.database.DataSourceService;
import org.wuyuhua.server.database.SqlService;
import org.wuyuhua.utils.CommonMethodUtils;

import java.util.Date;


@Service
public class SqlServiceImpl extends ServiceImpl<SqlMapper, SqlModel> implements SqlService {

    private final DataSourceService dataSourceService;

    public SqlServiceImpl(DataSourceService dataSourceService) {
        this.dataSourceService = dataSourceService;
    }

    @Override
    public Page<SqlModel> findSql(Integer pageSize, Integer pageNum, Long dataSourceId, String name, String startTime, String endTime, String dataSourceName) {
        Page<SqlModel> p = new Page<>(pageNum,pageSize);
        QueryWrapper<SqlModel> query = new QueryWrapper<>();

        if (StringUtils.hasText(dataSourceName)){
            DataSourceModel one = dataSourceService.getOne(new LambdaQueryWrapper<DataSourceModel>().eq(DataSourceModel::getDataSourceName, dataSourceName));
            dataSourceId = one.getId();
        }

        CommonMethodUtils.buildQuery(query,name,startTime,endTime);

        if (dataSourceId != null){
            query.eq("data_source_id",dataSourceId);
        }

        query.orderByDesc("create_time");
        return this.page(p, query);
    }

    @Override
    public SqlModel saveSqlModel(SqlModel model) {
        boolean isAdd = model.getId() == null;
        model.setUpdateTime(new Date());
        if (isAdd){
            // 新建名字
            String name = "新建查询";
            long count = count(new LambdaQueryWrapper<SqlModel>().eq(SqlModel::getDataSourceId,model.getDataSourceId()).likeRight(SqlModel::getName, name));
            model.setName(name+(count+1));
            model.setCreateTime(new Date());
            if (!save(model)) {
                throw new RuntimeException("保存失败,请检查参数和运行日志");
            }
        } else {
            SqlModel old = getById(model.getId());
            if (old == null) {
                throw new RuntimeException("未找到信息");
            }
            if (old.getContent().equals(model.getContent()) && old.getName().equals(model.getName()) && old.getSqlDesc().equals(model.getSqlDesc())) {
                throw new RuntimeException("未监测到改动");
            }
            if (!updateById(model)) {
                throw new RuntimeException("保存失败,请检查参数和运行日志");
            }
        }
        return model;
    }
}
