package org.wuyuhua.server.database;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.wuyuhua.model.database.po.SqlModel;

public interface SqlService extends IService<SqlModel> {
    /**
     * 查询当前数据源的sql列表
     * @param pageSize 分页参数
     * @param pageNum 分页参数
     * @param dataSourceId 数据源id
     * @param name 名称
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @param dataSourceName 数据源名称
     * @return Page<SqlModel>
     */
    Page<SqlModel> findSql(Integer pageSize, Integer pageNum, Long dataSourceId, String name, String startTime, String endTime, String dataSourceName);

    SqlModel saveSqlModel(SqlModel model);
}
