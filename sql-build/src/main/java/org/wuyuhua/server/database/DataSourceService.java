package org.wuyuhua.server.database;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.wuyuhua.model.database.po.DataSourceModel;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author 伍玉华
 */
public interface DataSourceService extends IService<DataSourceModel> {
    /**
     * 获取全部数据源列表
     * @param request 当前请求
     * @return List<DataSourceModel>
     *
     */
    List<DataSourceModel> findDataSourceAll(HttpServletRequest request);

    /**
     * 分页查询数据源列表
     * @param pageSize 页面大小
     * @param pageNum 页码
     * @param name 名称
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return Page<DataSourceModel></DataSourceModel>
     */
    Page<DataSourceModel> findDataSource(Integer pageSize, Integer pageNum, String name, String startTime, String endTime);

    /**
     * 保存数据源
     * @param model 数据源封装类
     * @param request 请求
     * @return DataSourceModel
     */
    DataSourceModel saveDataSource(DataSourceModel model,HttpServletRequest request);

    /**
     * 删除数据源
     * @param name 数据源名称
     * @param request 请求
     * @return DataSourceModel
     */
    DataSourceModel delDataSource(String name,HttpServletRequest request);

    /**
     * 关闭数据源
     * @param name 数据源
     */
    void close(String name);

    /**
     * 测试
     * @param dataBaseName 数据源名称
     */
    void test(String dataBaseName);

    /**
     * 获取当前数据源
     * @param request 请求
     * @return String
     */
    String getNowDataSource(HttpServletRequest request);

    /**
     * 设置datasource
     * @param name 数据源名称
     * @param request 请求
     */
    void setDatasource(String name,HttpServletRequest request);
}
