package org.wuyuhua.server.database.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.wuyuhua.common.exception.ServerException;
import org.wuyuhua.compent.DataBaseClient;
import org.wuyuhua.mapper.datasource.DataSourceMapper;
import org.wuyuhua.mapper.datasource.SqlMapper;
import org.wuyuhua.model.database.po.DataSourceModel;
import org.wuyuhua.model.database.po.SqlModel;
import org.wuyuhua.server.database.DataSourceService;
import org.wuyuhua.utils.CommonMethodUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@Service
public class DataSourceServiceImpl extends ServiceImpl<DataSourceMapper, DataSourceModel> implements DataSourceService {
    private final SqlMapper sqlMapper;
    private final DataBaseClient dataBaseClient;
    public DataSourceServiceImpl(SqlMapper sqlMapper, DataBaseClient dataBaseClient) {
        this.sqlMapper = sqlMapper;
        this.dataBaseClient = dataBaseClient;
    }

    @Override
    public List<DataSourceModel> findDataSourceAll(HttpServletRequest request) {
        // 查询全部数据源
        List<DataSourceModel> list = list();

        if (list.isEmpty()) {
            return list;
        }

        // 获取当前正在使用的数据源
        String nowUseDataSource = dataBaseClient.getNowDataSourceBySession().getDataBaseName();

        if (null == nowUseDataSource) {
            list.get(0).setIsUse(true);
        } else {
            for (DataSourceModel item : list) {
                item.setIsUse(item.getDataSourceName().equals(nowUseDataSource));
            }
        }
        return list;
    }

    @Override
    public Page<DataSourceModel> findDataSource(Integer pageSize, Integer pageNum, String name, String startTime, String endTime) {
        Page<DataSourceModel> page = new Page<>(pageNum, pageSize);
        QueryWrapper<DataSourceModel> query = new QueryWrapper<>();
        CommonMethodUtils.buildQuery(query, name, startTime, endTime);
        query.orderByDesc("create_time");
        return this.page(page, query);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public DataSourceModel saveDataSource(DataSourceModel model,HttpServletRequest request) {
        if (model == null) {
            return null;
        }
        if (model.getId() != null) {
            // save
            DataSourceModel oldObject = this.getById(model.getId());
            model.setUpdateTime(new Date());
            if (model.eq(oldObject)) {
                throw new RuntimeException("数据源信息没有变动");
            } else {
                this.updateById(model);
            }
            dataBaseClient.delDataSourceBySession(model.getDataSourceName(),request);
            dataBaseClient.addDataSource(model);
        } else {
            // 检查是否有重复的数据源
            DataSourceModel one = this.getOne(new LambdaQueryWrapper<DataSourceModel>()
                    .eq(DataSourceModel::getDataSourceName, model.getDataSourceName()));
            if (one != null) {
                throw new RuntimeException("数据源" + model.getDataSourceName() + "已经存在");
            }
            model.setCreateTime(new Date());
            model.setUpdateTime(model.getCreateTime());
            // insert
            this.save(model);
            dataBaseClient.addDataSource(model);
        }
        return model;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public DataSourceModel delDataSource(String name,HttpServletRequest request) {
        DataSourceModel dataSource = getOne(new LambdaQueryWrapper<DataSourceModel>().eq(DataSourceModel::getDataSourceName, name).last("limit 1"));
        if (dataSource != null) {
            removeById(dataSource.getId());
            sqlMapper.delete(new LambdaQueryWrapper<SqlModel>().eq(SqlModel::getDataSourceId, dataSource.getId()));
            dataBaseClient.delDataSourceBySession(dataSource.getDataSourceName(),request);
        }
        return dataSource;
    }

    @Override
    public void close(String name) {
        dataBaseClient.removeDataSource(name);
    }

    @Override
    public void test(String dataBaseName) {
        try {
            dataBaseClient.getDataSourceBuild(dataBaseName).testConnection();
        } catch (Exception e){
            throw new ServerException(e);
        }
    }

    @Override
    public String getNowDataSource(HttpServletRequest request) {
        return dataBaseClient.getNowDataSourceBySession().getDataBaseName();
    }

    @Override
    public void setDatasource(String name, HttpServletRequest request) {
        dataBaseClient.setNowDataSourceBySession(name);
    }

}
