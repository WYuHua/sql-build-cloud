package org.wuyuhua.datasource;

import com.zaxxer.hikari.HikariDataSource;
import org.wuyuhua.model.sqlnuild.dto.SearchResultDTO;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>DuckDB数据源配置</p>
 *
 * @author 伍玉华
 * @version V1.0.0
 * @date 2024/5/7 16:20
 */

public class DuckDbDataSourceBuild extends HikariDataSourceBuild {

    private static final String TABLES_SQL = "SELECT schema_name,table_name FROM duckdb_tables";
    private static final String FIELDS_SQL = "PRAGMA table_info('{#tableName}')";
    @Override
    public List<String> tables(){
        try {
            List<Map<String,Object>> tables = getJdbcTemplate().queryForList(TABLES_SQL);
            List<String> tableNames = new ArrayList<>();
            for (Map<String, Object> table : tables) {
                String schema = table.get("schema_name").toString();
                String tName = table.get("table_name").toString();
                tableNames.add(schema+"."+tName);
            }
            return tableNames;
        } finally {
            close();
        }
    }

    /**
     * 测试连接方法
     */
    @Override
    public void testConnection(){
        tables();
    }

    @Override
    public DataSource getDataSource() {
        if (this.dataSource instanceof HikariDataSource) {
            if (((HikariDataSource) this.dataSource).isClosed()) {
                initDataSource(this.model);
            }
        }
        return this.dataSource;
    }

    @Override
    public List<String> fields(String tableName) {
        try {
            List<Map<String, Object>> maps = getJdbcTemplate().queryForList(FIELDS_SQL.replace("{#tableName}",tableName));
            List<String> fieldNames = new ArrayList<>();
            for (Map<String, Object> map : maps) {
                fieldNames.add((String) map.get("name"));
            }
            return fieldNames;
        } finally {
            close();
        }
    }

    @Override
    public SearchResultDTO getDataBySql(String sql) {
        try {
            return super.getDataBySql(sql);
        } finally {
            close();
        }
    }
}
