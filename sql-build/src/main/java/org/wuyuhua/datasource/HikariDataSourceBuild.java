package org.wuyuhua.datasource;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.wuyuhua.model.database.po.DataSourceModel;

import javax.sql.DataSource;

/**
 * <p>HikariDataSource 构建</p>
 *
 * @author 伍玉华
 * @version V1.0.0
 * @date 2024/5/7 16:07
 */
@Slf4j
public class HikariDataSourceBuild extends AbstractDefaultSourceBuild{

    protected DataSourceModel model;
    protected DataSource dataSource;
    protected volatile JdbcTemplate jdbcTemplate;

    @Override
    public String getDataBaseName() {
        return this.model.getDataSourceName();
    }

    @Override
    public void initDataSource(DataSourceModel model) {
        HikariConfig config = new  HikariConfig();
        log.info("DruidDataSourceBuild initDataSource,nowModel:{},model:{}",this.model,model);
        config.setDriverClassName(model.getDriverClassName());
        config.setMaximumPoolSize(10);
        config.setMaxLifetime(1);
        config.setMinimumIdle(0);
        config.setJdbcUrl(model.getUrl());
        config.setUsername(model.getUsername());
        config.setPassword(model.getPassword());
        dataSource = new HikariDataSource(config);
        this.model = model;
    }

    @Override
    public JdbcTemplate getJdbcTemplate() {
        synchronized (this){
            if (this.jdbcTemplate == null){
                synchronized (this){
                    if (jdbcTemplate == null){
                        jdbcTemplate = new JdbcTemplate(getDataSource());
                    } else {
                        return jdbcTemplate;
                    }
                }
            }
        }
        return jdbcTemplate;
    }

    @Override
    public DataSource getDataSource() {
        return this.dataSource;
    }

    @Override
    public void close(){
        if (getDataSource() instanceof HikariDataSource){
            ((HikariDataSource) getDataSource()).close();
        }
        jdbcTemplate = null;
    }
}
