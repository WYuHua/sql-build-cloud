package org.wuyuhua.datasource;

/**
 * 数据源枚举
 * @author 伍玉华
 */
public enum DataSourceEnum {
    /**
     * MYSQL_JDBC MYSQL_CJ_JDBC_DRIVER
     */
    MYSQL_CJ_JDBC_DRIVER("com.mysql.cj.jdbc.Driver",DruidDataSourceBuild.class),

    DUCK_DB_JDBC_DRIVER("org.duckdb.DuckDBDriver",DuckDbDataSourceBuild.class);

    /**
     * 驱动名称
     */
    public final String driverName;
    public final Class<?> build;

    DataSourceEnum(String driverName, Class<?> clazz) {
        this.driverName = driverName;
        this.build = clazz;
    }
}
