package org.wuyuhua.datasource;

import com.alibaba.druid.pool.DruidDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.wuyuhua.model.database.po.DataSourceModel;

import javax.sql.DataSource;

/**
 * <p>DruidDataSource 构建</p>
 *
 * @author 伍玉华
 * @version V1.0.0
 * @date 2024/5/7 16:03
 */
@Slf4j
public class DruidDataSourceBuild extends AbstractDefaultSourceBuild {
    private String dataSourceName;
    private DataSource dataSource;
    private volatile JdbcTemplate jdbcTemplate;

    @Override
    public String getDataBaseName() {
        return this.dataSourceName;
    }

    @Override
    public void initDataSource(DataSourceModel model) {
        log.info("DruidDataSourceBuild initDataSource,dataSourceName:{},model:{}",dataSourceName,model);
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setName(model.getDataSourceName());
        druidDataSource.setDriverClassName(model.getDriverClassName());
        druidDataSource.setUrl(model.getUrl());
        druidDataSource.setUsername(model.getUsername());
        druidDataSource.setPassword(model.getPassword());
        druidDataSource.setInitialSize(0);
        druidDataSource.setMinIdle(0);
        druidDataSource.setMaxActive(1);
        // 获取连接的最大等待时间: 5秒
        druidDataSource.setMaxWait(5000);
        // 检测连接是否可用的频率: 60秒
        druidDataSource.setTimeBetweenConnectErrorMillis(60000);
        // 连接最小生存时间: 10分钟
        druidDataSource.setMinEvictableIdleTimeMillis(600000);
        // 连接最大生存时间: 30分钟
        druidDataSource.setMaxEvictableIdleTimeMillis(1800000);
        druidDataSource.setValidationQuery("SELECT 1 FROM DUAL");
        druidDataSource.setTestWhileIdle(true);
        druidDataSource.setTestOnBorrow(false);
        druidDataSource.setTestOnReturn(false);
        druidDataSource.setPoolPreparedStatements(true);
        druidDataSource.setMaxPoolPreparedStatementPerConnectionSize(1);
        dataSource = druidDataSource;
        dataSourceName = model.getDataSourceName();
    }

    @Override
    public JdbcTemplate getJdbcTemplate() {
        synchronized (this){
            if (this.jdbcTemplate == null){
                synchronized (this){
                    if (jdbcTemplate == null){
                        jdbcTemplate = new JdbcTemplate(getDataSource());
                    } else {
                        return jdbcTemplate;
                    }
                }
            }
        }
        return jdbcTemplate;
    }

    @Override
    public DataSource getDataSource() {
        return this.dataSource;
    }

    @Override
    public void close() {
        if (dataSource instanceof DruidDataSource) {
            ((DruidDataSource) dataSource).close();
        }
        jdbcTemplate = null;
    }
}
