package org.wuyuhua.datasource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.wuyuhua.model.database.po.DataSourceModel;
import org.wuyuhua.model.sqlnuild.dto.SearchResultDTO;

import javax.sql.DataSource;
import java.util.List;

/**
 * <p>DuckDB 数据源</p>
 *
 * @author 伍玉华
 * @version V1.0.0
 * @date 2024/5/7 15:48
 */

public interface DataSourceBuild  {

    /**
     * 获取数据源名称
     * @return String
     */
    String getDataBaseName();
    /**
     * 构建数据源 数据源对象
     * @param model model
     */
    void initDataSource(DataSourceModel model);

    /**
     * 测试sql
     * @return 测试Sql
     */
    void testConnection();

    /**
     * 获取数据表列表
     * @return List
     */
    List<String> tables();

    /**
     * 获取属性列表
     * @param tableName 表名
     * @return List
     */
    List<String> fields(String tableName);

    /**
     * 根据sql查询数据
     * @param sql sql
     * @return 查询结果
     */
    SearchResultDTO getDataBySql(String sql);
    /**
     * getJdbcTemplate
     * @return JdbcTemplate
     */

    JdbcTemplate getJdbcTemplate();

    /**
     * 获取数据源
     * @return DataSource
     */
    DataSource getDataSource();

    /**
     * 释放资源
     */
    void close();

    /**
     * 获取数据源建表ddl语句
     * @return ddl
     */
    Object getCreateTableDdl(String tableName);
}
