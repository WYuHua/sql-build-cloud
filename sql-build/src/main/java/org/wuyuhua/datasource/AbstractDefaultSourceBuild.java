package org.wuyuhua.datasource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.wuyuhua.model.database.po.DataSourceModel;
import org.wuyuhua.model.sqlnuild.dto.ColumnDTO;
import org.wuyuhua.model.sqlnuild.dto.SearchResultDTO;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>默认数据源构建</p>
 *
 * @author 伍玉华
 * @version V1.0.0
 * @date 2024/5/7 18:14
 */

public abstract class AbstractDefaultSourceBuild implements DataSourceBuild {

    private static final String TABLES_SQL = "SHOW TABLES";
    private static final String TABLE_DDL_SQL = "show create table ";
    private static final String FIELDS_SQL = "SHOW COLUMNS FROM ";

    /**
     * 获取数据源名称
     * @return String
     */
    @Override
    public abstract String getDataBaseName();

    /**
     * 初始数据源
     * @param model model
     */
    @Override
    public abstract void initDataSource(DataSourceModel model);

    /**
     * 测试连接方法
     */
    @Override
    public void testConnection(){
        getJdbcTemplate().queryForList("SELECT 1 FROM DUAL",Integer.class);
    }

    @Override
    public List<String> tables() {
        return getJdbcTemplate().queryForList(TABLES_SQL, String.class);
    }

    @Override
    public List<String> fields(String tableName) {
        List<String> result = new ArrayList<>();
        List<Map<String, Object>> maps = getJdbcTemplate().queryForList(FIELDS_SQL + tableName);
        for (Map<String, Object> map : maps) {
            result.add(map.get("Field").toString());
        }
        return result;
    }

    @Override
    public SearchResultDTO getDataBySql(String sql) {
        List<Map<String, Object>> maps = getJdbcTemplate().queryForList(sql);
        List<ColumnDTO> columns = new ArrayList<>();
        if (!maps.isEmpty()) {
            Map<String, Object> stringObjectMap = maps.get(0);
            columns = new ArrayList<>();
            for (String key : stringObjectMap.keySet()) {
                columns.add(new ColumnDTO(key, key));
            }
        }
        return new SearchResultDTO(maps, columns);
    }

    @Override
    public Object getCreateTableDdl(String tableName) {
        return getJdbcTemplate().queryForMap(TABLE_DDL_SQL + tableName);
    }

    /**
     * 获取jdbcTemplate
     * @return JdbcTemplate
     */
    @Override
    public abstract JdbcTemplate getJdbcTemplate();

    /**
     * 获取数据源
     * @return DataSource
     */
    @Override
    public abstract DataSource getDataSource();

    /**
     * 关闭数据源
     */
    @Override
    public abstract void close();
}
