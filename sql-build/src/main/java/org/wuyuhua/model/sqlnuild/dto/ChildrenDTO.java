package org.wuyuhua.model.sqlnuild.dto;

import lombok.Data;

import java.util.List;

@Data
public class ChildrenDTO {
    private ValueDTO value;
    private String label;
    private List<String> fields;
}
