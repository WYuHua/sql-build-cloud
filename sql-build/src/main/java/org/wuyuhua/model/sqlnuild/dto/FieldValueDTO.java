package org.wuyuhua.model.sqlnuild.dto;

import lombok.Data;

@Data
public class FieldValueDTO {

    private String value;
    private String label;
}
