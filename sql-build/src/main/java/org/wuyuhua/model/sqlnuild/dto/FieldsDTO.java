package org.wuyuhua.model.sqlnuild.dto;

import lombok.Data;

import java.util.List;

@Data
public class FieldsDTO {
    private Object[] value;
    private String label;
    private List<FieldValueDTO> children;

}
