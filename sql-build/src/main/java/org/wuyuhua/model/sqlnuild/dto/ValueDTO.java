package org.wuyuhua.model.sqlnuild.dto;

import lombok.Data;

@Data
public class ValueDTO {

    private Integer sqlId;
    private String value;

    public ValueDTO() {
    }

    public ValueDTO(Integer sqlId, String value) {
        this.sqlId = sqlId;
        this.value = value;
    }
}
