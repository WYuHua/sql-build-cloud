package org.wuyuhua.model.sqlnuild.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 数据表或子查询 参数
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TableSearchDTO {
    /*名字*/
    private String name;

    /*别名*/
    private String nickname;

    /*1.数据表,2.子查询*/
    private Integer type;

    /*子查询id*/
    private Integer sqlId;

    public String getName() {
        if (this.sqlId == null){
            return "";
        }
        if (this.sqlId == -1){
            return this.name;
        } else {
            return "#{" + this.name + "}";
        }
    }
}
