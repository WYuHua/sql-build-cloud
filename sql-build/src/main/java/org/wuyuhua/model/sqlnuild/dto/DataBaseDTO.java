package org.wuyuhua.model.sqlnuild.dto;

import lombok.Data;

import java.util.List;

@Data
public class DataBaseDTO {
    private List<TableDTO> tables;
    private List<FieldsDTO> fields;
}
