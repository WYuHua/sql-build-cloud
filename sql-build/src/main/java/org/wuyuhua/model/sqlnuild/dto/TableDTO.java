package org.wuyuhua.model.sqlnuild.dto;

import lombok.Data;

import java.util.List;

@Data
public class TableDTO {

    private String value;
    private String label;
    private List<ChildrenDTO> children;
}
