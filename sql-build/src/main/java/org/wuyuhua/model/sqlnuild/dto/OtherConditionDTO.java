package org.wuyuhua.model.sqlnuild.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 其他条件
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OtherConditionDTO {

    private String typeName;
    private List<FieldSearchDTO> fields;
    private String content;
}
