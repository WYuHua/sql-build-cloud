package org.wuyuhua.model.sqlnuild.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 查询字段 参数
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FieldSearchDTO {

    /*字段所在数据表或子查询参数列表中的下标*/
    private TableSearchDTO table;
    /*字段名称*/
    private String field;
    /*别名*/
    private String nickname;
    /*自定义*/
    private String custom;
}
