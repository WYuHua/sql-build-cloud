package org.wuyuhua.model.sqlnuild.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * sql构建入参封装类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SqlBuildDTO {

    /*数据表和子查询*/
    private List<TableSearchDTO> tableSearch;
    /*字段查询*/
    private List<FieldSearchDTO> fieldSearch;
    /*数据表关联关系*/
    private List<TableAssociationDTO> tableAssociation;
    /*筛选关联关系*/
    private List<FilterSearchDTO> filterSearch;
    /*筛选关系*/
    private List<FilterAssociationDTO> filterAssociation;
    /*其他条件*/
    private List<OtherConditionDTO> otherCondition;
}
