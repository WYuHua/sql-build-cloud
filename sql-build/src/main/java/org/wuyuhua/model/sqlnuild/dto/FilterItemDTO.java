package org.wuyuhua.model.sqlnuild.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 筛选条件
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FilterItemDTO {

    /*查询字段 参数*/
    private FieldSearchDTO left;
    /*符号*/
    private String symbol;
    /*查询字段 参数*/
    private FieldSearchDTO right;
    /*自定义右侧内容*/
    private String rightCustom;
    /*子查询id*/
    private TableSearchDTO sql;
    /*连接符*/
    private String connector;
}
