package org.wuyuhua.model.sqlnuild.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 数据表关联关系
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TableAssociationDTO {
    /*数据表或子查询*/
    private TableSearchDTO master;
    /*数据表或子查询*/
    private TableSearchDTO slave;
    /*连接类型*/
    private String connType;
    /*连接规则*/
    private FieldSearchDTO left;
    private FieldSearchDTO right;
}
