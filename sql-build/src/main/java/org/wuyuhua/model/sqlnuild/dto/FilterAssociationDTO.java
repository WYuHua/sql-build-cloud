package org.wuyuhua.model.sqlnuild.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 筛选关系
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FilterAssociationDTO {

    private String value;
    private String label;
    /*1.and 0.or*/
    private Integer type;
    /*子节点*/
    private List<FilterAssociationDTO> children;
}
