package org.wuyuhua.model.sqlnuild.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 筛选关联关系
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FilterSearchDTO {
    /*名称*/
    private String name;
    /*筛选条件*/
    private List<FilterItemDTO> filters;
}
