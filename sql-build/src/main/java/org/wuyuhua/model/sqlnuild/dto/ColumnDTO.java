package org.wuyuhua.model.sqlnuild.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ColumnDTO implements Serializable {
  private static final long serialVersionUID = -2534677876420639538L;

  private String id;
  private String label;
}
