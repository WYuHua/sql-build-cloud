package org.wuyuhua.model.sqlnuild.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author 伍玉华
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SearchResultDTO implements Serializable {
  private static final long serialVersionUID = -4866956547977773807L;

  private List<Map<String,Object>> dataValue;
  private List<ColumnDTO> columns;
}
