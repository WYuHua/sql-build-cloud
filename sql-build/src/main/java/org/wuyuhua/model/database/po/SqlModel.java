package org.wuyuhua.model.database.po;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor(force = true)
@EqualsAndHashCode(callSuper = true)
@TableName("t_sql")
public class SqlModel extends Model<SqlModel> implements Serializable {
    private static final long serialVersionUID = -6209260071316908445L;

    @TableId(type = IdType.AUTO)
    private Long id;

    private String name;

    private String sqlDesc;

    private String content;

    private Long dataSourceId;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}

