package org.wuyuhua.model.database.po;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor(force = true)
@EqualsAndHashCode(callSuper = true)
@TableName("t_datasource")
public class DataSourceModel extends Model<DataSourceModel> implements Serializable {


    private static final long serialVersionUID = 4374803677045746934L;
    @TableId(type = IdType.AUTO)
    private Long id;
    @NotNull(value = "数据源名称不能为空")
    private String dataSourceName;
    @NotNull(value = "连接地址不能为空")
    private String url;
    private String username;
    private String password;
    private String driverClassName;
    @TableField(exist = false)
    private Boolean isUse;
    @TableField(exist = false)
    private String uuid;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    public boolean eq(DataSourceModel model1){
        if (model1!=null){
            return model1.getId() == null
                    && !model1.getDataSourceName().equals(this.dataSourceName)
                    && !model1.getUrl().equals(this.url)
                    && !model1.getUsername().equals(this.username)
                    && !model1.getPassword().equals(this.password)
                    && !model1.getDriverClassName().equals(this.getDriverClassName());
        } else {
            return false;
        }
    }
}
