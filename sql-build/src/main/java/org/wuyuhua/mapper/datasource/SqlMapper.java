package org.wuyuhua.mapper.datasource;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.wuyuhua.model.database.po.SqlModel;

@Mapper
public interface SqlMapper extends BaseMapper<SqlModel> {
}
