package org.wuyuhua.mapper.datasource;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.wuyuhua.model.database.po.DataSourceModel;

@Mapper
public interface DataSourceMapper extends BaseMapper<DataSourceModel> {
}
