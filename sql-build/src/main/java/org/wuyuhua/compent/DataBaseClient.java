package org.wuyuhua.compent;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.wuyuhua.common.exception.ServerException;
import org.wuyuhua.datasource.DataSourceBuild;
import org.wuyuhua.datasource.DataSourceEnum;
import org.wuyuhua.datasource.DruidDataSourceBuild;
import org.wuyuhua.mapper.datasource.DataSourceMapper;
import org.wuyuhua.model.database.po.DataSourceModel;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author 伍玉华
 */
@Slf4j
@Component
public class DataBaseClient {
    private static final String NOW_DATASOURCE_KEY = "data_source_name";
    private String nowDataSource = "";
    /**
     * 数据源池
     */
    private final Map<String, DataSourceBuild> dataSources = new ConcurrentHashMap<>(64);

    public DataBaseClient(DataSourceMapper dataSourceMapper) {

        List<DataSourceModel> list = dataSourceMapper.selectList(new LambdaQueryWrapper<>());
        for (DataSourceModel dataSourceModel : list) {
            addDataSource(dataSourceModel);
        }
        log.info("Bean DataBaseClient init successfully");
    }

    public DataSourceBuild getDataSourceBuild(String dataBaseName) {
        DataSourceBuild dataSource = dataSources.get(dataBaseName);
        if (null == dataSource) {
            throw new ServerException("database is not found");
        }
        log.info("getDataSourceBuild");
        return dataSource;
    }

    public void addDataSource(DataSourceModel model) {
        log.info("thread: {} add DynamicDataSource :{}", Thread.currentThread().getName(),model.getDataSourceName());
        DataSourceBuild build = null;
        for (DataSourceEnum value : DataSourceEnum.values()) {
            if (value.driverName.equals(model.getDriverClassName())) {
                Class<?> clazz = value.build;
                try {
                    build = (DataSourceBuild) clazz.newInstance();
                } catch (InstantiationException | IllegalAccessException e) {
                    log.debug("addDataSource::{}",model,e);
                }
                break;
            }
        }
        // 默认使用Druid
        if (null == build){
            build = new DruidDataSourceBuild();
        }

        build.initDataSource(model);
        dataSources.put(model.getDataSourceName(), build);
        nowDataSource = model.getDataSourceName();
    }
    public void removeDataSource(String dataBaseName) {
        DataSourceBuild dataSourceBuild = dataSources.get(dataBaseName);
        if (null == dataSourceBuild) {
            return;
        }
        dataSources.remove(dataBaseName);
        dataSourceBuild.close();
    }


    public DataSourceBuild getNowDataSourceBySession() {
        return getDataSourceBuild(nowDataSource);
    }


    /**
     * 获取当前使用的数据源
     */
    public void setNowDataSourceBySession(String dataSourceName) {
        nowDataSource = dataSourceName;
    }

    public void delDataSourceBySession(String dataSourceName,HttpServletRequest request) {
        String nowDataSourceBySession = getNowDataSourceBySession().getDataBaseName();
        if (null != nowDataSourceBySession) {
            request.getSession().removeAttribute(NOW_DATASOURCE_KEY);
        }
        removeDataSource(dataSourceName);
    }
}
